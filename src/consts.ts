// Place any global data in this file.
// You can import this data from anywhere in your site by using the `import` keyword.

export const SITE_TITLE = 'Tommy Morgan';
export const SITE_DESCRIPTION = 'Tommy Morgan is a software engineer based in Austin.';
