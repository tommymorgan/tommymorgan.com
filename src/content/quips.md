You'll catch more flies with honey than vinegar.  You can catch even more with bullshit, which explains a lot. — Me

If code is hard to test that's not the test's fault. — Me

Be a gardener, not a firefighter. — Me

I'm not a doctor, but I'm pretty sure that you'd feel better if you got that stick out of your ass. — Me

Testing shows the presence, not the absence of bugs. — Dijkstra

Customer research is a flashlight when we feel like we're in the dark — Jason Fried

Never confuse motion with progress.

An ounce of prevention is worth a pound of cure.

The more your tests resemble the way your software is used, the more confidence they can give you.

Just because you can doesn't mean you should.

The easiest way to learn is the hard way. — Me

There's no crying in baseball and there are no ties in prioritization.

Lack of planning on your part does not constitute an emergency on mine.

Hope is not a strategy.

Panic is not a plan.

The purpose of bureaucracy is to compensate for incompetence and lack of discipline. — Good to Great

With just weeks of work you can save hours of planning.

Be the role, get the job. — Steve McMichael (passing along wisdom he had previously been gifted)

Bad code slows us down. And why did we write bad code? Because we had to go fast. (Uncle Bob)

The leadership you work under is more important than the perks your company offers. — First, Break All the Rules

Conventional wisdom is conventional for a reason: It is easier. — First, Break All the Rules

Batch size is a leading indicator of cycle time. ([citation](https://lucasfcosta.com/2022/07/19/finish-what-you-start.html))

Don't get so busy draining the flood that you aren't able to fix the leak.

The goal is long term success not short term gratification.

You don't have to be faster than the bear. You just have to be faster than your slowest friend.

The solution to ambiguity is not more policy. It's good judgment.

Context is worth 100 IQ points. — Keith Zoellner

You can't fit 10 pounds of shit in a 5 pound bag no matter how small the turds are.

You don't get to shit in a room and leave. — Keith Zoellner

You can't have conviction without rationale. — Keith Zoellner

When you find yourself in a hole… stop digging.

Necessity is the mother of invention. Laziness is the father.

Talk less. Say more.

The clarity of your guidance gets measured at the other person's ear, not at your mouth.

What's the least amount of work we need to do to learn the next most important thing?

No plan survives contact with the enemy.

Tact is the ability to tell someone to go to hell in such a way that they look forward to the trip. — Winston Churchill

Never half-ass two things, whole-ass one thing. — Ron Swanson

Code tells you how; Comments tell you why.

Branching is a high interest integration credit card.

Heroism leads to zeroism.

The ideal story is one that cannot be broken down any further.

Think big, start small.

Every responsibility a team cedes to increase its focus becomes a new cross-team dependency.

The faintest pencil is better than the sharpest memory.

You can't learn if you can't fail.

I didn't fail, I learned.

If it hurts, do it more often.

Simplify, automate, repeat.

The goal is to get it right, not to be right.

Improvement of daily work is more important than daily work.

Left unchecked, technical debt will ensure that the only work that gets done is unplanned work!

What can you do in your organization to add a little rudder far from the rocks to prevent needing a lot of rudder next to the rocks? — Turn the Ship Around!

Headcount is a vanity metric.

The best time to plant a tree was 20 years ago. The second best time is now.

Be skeptical; don't be cynical.

Aim for perfection, accept excellence.

Good judgment comes from experience. Experience comes from bad judgment.

Watch the work not the workers.

this should never happen.

People think focus means saying yes to the thing you've got to focus on. But that's not what it means at all. It means saying no to the hundred other good 
ideas. — Steve Jobs

Culture is a reflection of values.

Leaders must sometimes make lonely decisions.

Disagree without being disagreeable.

Rework steals focus. Lack of focus causes rework.

Haste makes waste.

Healthy teams aren't built in a day. But they are built each day.

Brook's Law: Adding more people to a late project will make it later.

Goodhart's Law: Any metric if made to be a goal will cease to be a good metric.

Repetition is the key to learning. — Keith Zoellner

A smooth sea never made a skilled sailor.

Greatness is idiosyncratic.

You can't half-ass your way to excellence.

Slow is smooth and smooth is fast.
