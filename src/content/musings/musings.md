---
pubDate: "Jan 26, 2025 21:07"
---

I want a place to publish thoughts without having to write a whole article and without participating in a walled garden.  The open web is better so that's where I'm gonna hang out.
