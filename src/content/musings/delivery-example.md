---
pubDate: "Jan 28, 2025 11:28:00"
---

Here are some real examples of what I was talking about in my previous post.

* I'd like a different theme for this site but posting content is more important.
* My photo on the about page is way bigger than I would like but posting content is more important.
* I'm working on an app for myself and I'm not getting bogged down with things like auth and fancy styling.  It's not live yet, so that stuff's just a distraction.  I have placeholder tests so auth will be handled when the time is right.  For now what I need is to figure out what the app should do, how I want it to behave, etc.
* I'm a team of one on this site, so I don't need heavy processes.  I commit to main and push then my CD pipeline does the rest.  A few seconds later the updated site is live. 🚀