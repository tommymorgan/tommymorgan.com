---
pubDate: "Jan 28, 2025 00:01:00"
---

Some recent work on a couple of side projects has reminded me of the importance of keeping things simple.  Maximize meaningful effort, minimize the rest, and deliver!  Delivery is fun!  Be wary of over-engineering.