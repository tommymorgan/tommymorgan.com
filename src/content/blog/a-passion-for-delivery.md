---
title: 'A Passion for Delivery'
description: 'I love shipping product. Throughout my career, I’ve worked on great teams, learned hard lessons, and developed a deep passion for effective delivery. This is my journey through deployment processes, leadership challenges, and the pursuit of shipping excellence.'
pubDate: 'Nov 11, 2023'
# heroImage: '/blog-placeholder-3.jpg'
---

I love shipping product.

Throughout my career I've had the good fortune to work on fun products with smart people and under excellent leadership. I've always taken pride in the work I do and I want customers to get real value from it and for customers to get value you have to ship.

I've worked at companies small and large and I've seen deployment processes ranging from "edit an application file directly on the server" to "zip everything up, write a multi-page document explaining exactly what hardware to deploy to, where to place the files, yada yada yada, send all that to someone across the country and be ready to hop on the phone when that silliness inevitably fails" to "click the merge button and it will be tested and in production shortly".

It was during my time at HomeAway that I learned that last one and it sparked a deep interest in learning how and why the process worked so well. I devoured books, blog posts, videos, podcasts, and wisdom from mentors. After over four years at HomeAway I wanted to take what I'd learned and apply it to a new challenge.

I don't quite know how to describe the time that followed, but for now I'll just call them my lost years. There was some success in there but not nearly what I expected. Fortunately there are lessons to be learned even from the tough times and I met some great people along the way.

Perhaps the biggest lesson I learned was that I had to do a much better job vetting the leadership and culture of the companies I was considering to make sure we were philosophically aligned on how to build and ship. If I hadn't learned that lesson before, I certainly learned it when one day during a 1:1 with my CTO he led off with "Since you don't have a passion for delivery…" 😮. What followed was a suggestion that I move into a new role that he thought better aligned with my skills and interests. I accepted because I have a family to support but I've never been so insulted in my career and started planning my exit. There were certainly *many* other good reasons to leave, but that one sealed the deal.

That company changes priorities more often than the average person changes underwear. If you want to ship you have to focus. You take a vision and break it into smaller goals. You make those goals ambitious, but realistic and you prioritize them. You must accept that important and prioritized are not the same thing. So when I pushed back on those lesser whims it wasn't because I didn't have a passion for delivery, it was because I did.

I love shipping product. I left that company and joined one where we ship all the time. And it feels great. 