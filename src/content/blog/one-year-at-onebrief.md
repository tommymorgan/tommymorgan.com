---
title: 'One Year at Onebrief'
description: 'Reflecting on my first year at Onebrief—how I found the right team, aligned with great leadership, and rekindled my passion for building. From past struggles to a thriving culture focused on delivery, this journey has been transformative.'
pubDate: 'Nov 14, 2023'
# heroImage: '/blog-placeholder-4.jpg'
---

 Today is my first anniversary at Onebrief and I wanted to express how grateful I am to be part of our outstanding team. As I mentioned in my [previous post](https://tommymorgan.com/blog/a-passion-for-delivery), I had a stretch of a few years that just weren't fun. Then in March 2022 Rafa, Onebrief's CTO, found me on [workatastartup.com](https://workatastartup.com/) and we started talking. The opportunity sounded interesting.

They were looking for a principal engineer to lead a team and Rafa and I seemed well-aligned philosophically. I had recently read [Why we don’t use a staging environment](https://web.archive.org/web/20220403124147/https://squeaky.ai/blog/development/why-we-dont-use-a-staging-environment) and among other opinions I expressed, I mentioned that article and why I agreed with the premise. Rafa seemed interested in trying it and I proceeded in the interview process and passed the technical bar.

As I mentioned in my previous post, after HomeAway I wanted to take what I'd learned and apply it to a new challenge. My goal was to set myself on a path toward a CTO role and I took a director level position in pursuit of that goal. Nothing is black and white, but I ended up having three jobs in a row where I had a big title but not much authority. It left me in a place where I couldn't execute on the things I knew would make a difference. Meanwhile, my son was diagnosed with autism and a variety of other things and the manager's schedule was not conducive to my family's needs.

In early April I got a call from a friend that another friend/former coworker had died. He was only a couple years older than me and had a family. It hit me pretty hard and caused me to take pause and assess whether I was doing the right thing by my family. I contacted Rafa and "pressed pause" on the interview process. Over the next several months I interviewed with a handful of other companies and was particularly excited about Netflix.

For better or worse things didn't pan out at Netflix and around October 2022 I emailed Rafa and asked how the staging experiment had gone. He said something along the lines of "It was a catastrophe!" and that's the moment I knew Onebrief was where I needed to be because I was confident that our alignment was real and that he was willing to buck conventional wisdom while at the same time being pragmatic. Luckily they still had a spot for me and I'm so happy they did. I'm loving being a builder again and one day I probably will want to re-focus on that CTO goal and my time here will make me a better candidate. The leadership team here is the strongest I've ever worked with, I'm surrounded by folks who are smarter than me, and I get to work on interesting problems.

We have a great team, we're building a great product, and the culture is out of this world. We stay busy but never on busy work. We're laser-focused on delivering value to our customers and we avoid the traps that most companies fall into such as shifting priorities and meeting constantly. We have heavy doses of 37signals and Netflix culture and I love it.

Rafa, Grant, and everyone else at Onebrief, thank you. I'm so happy to be a part of this incredible team and I look forward to the next year and beyond. 