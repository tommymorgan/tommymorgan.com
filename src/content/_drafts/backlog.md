* Backlogs - They are inventory
* Things I suck at
  * Going to bed on time (even though I know that's counter productive)
  * Being healthy even though I preach "if it hurts do it more often"
* Code ownership
* Laws
  * Amdahl
  * Goodhart
* Party tricks "rebuttal" - https://www.seangoedecke.com/party-tricks/
* Sacred cows - Link to the spraying gorillas post
  * tickets
  * PRs
* Ownership vs accountability
* I don't have time to test
* Conventional thinking
* TDD
* Keep it simple (this blog for instance)
* Type safety theater
  * compile time vs runtime
  * Hiding errors and making things worse
* Cargo cult
  * Agile